#include <avr/io.h>
#include "Communications/UART.h"
#include "Communications/LED.h"
#include "Interpreters/TypeInterpreter.h"

#define F_CPU 16000000  // system frequency from the system
#define BAUD 9600  // bits per second
#define MY_UBRR (FOSC/16/BAUD) - 1  // UBRRn Value bits per second

typedef void (*myAction)(uint8_t);

/**
 * Sets up the system
 */
void initialize() {
	/* Initialize the UART Buffer Channel */
	UART.init(BAUD, true);
	LED.setAsOutput();
	LED.turnOff();
}

/**
 * Prints error
 */
void printError(uint8_t some) {
	char error[] = "\nError\n";
	UART.newLine();
	UART.write(error);
}

/**
 * Function factory
 */
myAction getAction(int action) {
	myAction some;
	switch (action) {
	case flashCase:
		some = LED.flash;
		break;
	case lowerCase:
	case upperCase:
		some = UART.append;
		break;
	case errorCase:
	default:
		some = printError;
		break;
	};
	return some;
}

/**
 * Start here!
 */
int main(void) {
	/* set up system */
	initialize();
	uint8_t exit = 0;
	uint8_t received = 0;
	uint8_t interpreted = 0;
	myAction action;
	int actionType = 0;

	do {
		received = UART.get();
		interpreted = TypeInterpreter.interpretAscii(received);
		actionType = TypeInterpreter.translateAscii(received);
		action = getAction(actionType);
		(*action)(interpreted); // returns void
	}while(exit != 1);
}
