/*
 * TypeInterpreter.c
 *
 *  Created on: 14 Nov 2016
 *      Author: asolis
 */

#include "TypeInterpreter.h"
#include <stdio.h>

#define UPPER_CASE_CONVERTER 0xDF

/**
 * Interprets the data based on ASCII
 */
uint8_t interpretAscii(uint8_t data) {
	if (AsciiEvaluator.isAsciiNumber(data) == true) {
		return data & NUMBER_EXTRACTOR;
	}else if (AsciiEvaluator.isLowerCase(data) == true) {
		return data & UPPER_CASE_CONVERTER;
	}else if (AsciiEvaluator.isUpperCase(data) == true) {
		return data;
	}
	return ERROR_DATA; // return error
}

/**
 * Strategy to choose as an integer mapped
 */
int translateAscii(uint8_t data) {
	if (AsciiEvaluator.isAsciiNumber(data) == true) {
		return flashCase;
	}else if (AsciiEvaluator.isLowerCase(data) == true) {
		return lowerCase;
	}else if (AsciiEvaluator.isUpperCase(data) == true) {
		return upperCase;
	}
	return errorCase; // return error
}

const struct TypeInterpreter TypeInterpreter = {
		.translateAscii = translateAscii,
		.interpretAscii = interpretAscii
};
