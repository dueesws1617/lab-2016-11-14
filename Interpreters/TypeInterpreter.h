/*
 * TypeInterpreter.h
 *
 *  Created on: 16 Nov 2016
 *      Author: asolis
 */

#ifndef INTERPRETERS_TYPEINTERPRETER_H_
#define INTERPRETERS_TYPEINTERPRETER_H_

#include <avr/io.h>
#include <inttypes.h>
#include "Evaluators/AsciiEvaluator.h"

#define ERROR_DATA 0x0

/**
 * Mapped actions enumeration
 */
enum mappedActions {
	flashCase,
	lowerCase,
	upperCase,
	errorCase
};

/**
 * Type Interpreter
 * http://ascii.cl
 */
struct TypeInterpreter {
	uint8_t(*interpretAscii)(uint8_t data);
	int(*translateAscii)(uint8_t data);
};

extern const struct TypeInterpreter TypeInterpreter;

#endif /* INTERPRETERS_TYPEINTERPRETER_H_ */
