/*
 * AsciiEvaluator.c
 *
 *  Created on: 17 Nov 2016
 *      Author: asolis
 */


#include "AsciiEvaluator.h"

/**
 * Returns 1 if it is a valid ASCII number, else 0
 */
int isAsciiNumber(uint8_t data) {
	return ((data >> 4) == NUMBER) & ((data & NUMBER_EXTRACTOR) <= MAX_NUMBER);
}

/**
 * Returns 1 if the character is a lower case
 */
int isLowerCase(uint8_t data) {
	return ((data >> 5) == LOWER_CASE);
}

/**
 * Returns 1 if the character is an upper case
 */
int isUpperCase(uint8_t data) {
	return ((data >> 5) == UPPER_CASE);
}

/**
 * Build the structure
 */
const struct AsciiEvaluator AsciiEvaluator = {
		.isAsciiNumber = isAsciiNumber,
		.isLowerCase = isLowerCase,
		.isUpperCase = isUpperCase
};
