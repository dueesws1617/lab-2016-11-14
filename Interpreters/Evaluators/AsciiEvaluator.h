/*
 * AsciiEvaluator.h
 *
 *  Created on: 17 Nov 2016
 *      Author: asolis
 */

#ifndef INTERPRETERS_EVALUATORS_ASCIIEVALUATOR_H_
#define INTERPRETERS_EVALUATORS_ASCIIEVALUATOR_H_

#include <avr/io.h>
#include <inttypes.h>

#define NUMBER 0x3
#define MAX_NUMBER 0x9
#define NUMBER_EXTRACTOR 0x0F
#define LOWER_CASE 0x3
#define UPPER_CASE 0x2

/**
 * Boolean enumeration
 */
enum boolean {
	false,
	true
};

/**
 * ASCII evaluator structure
 */
struct AsciiEvaluator {
	int(*isAsciiNumber)(uint8_t data);
	int(*isLowerCase)(uint8_t data);
	int(*isUpperCase)(uint8_t data);
};

extern const struct AsciiEvaluator AsciiEvaluator;

#endif /* INTERPRETERS_EVALUATORS_ASCIIEVALUATOR_H_ */
