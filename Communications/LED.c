/**
 * LED.c
 *
 * 	Created on: 14 Nov 2016
 * 		Author: asolis
 */

#include "LED.h"
#include <stdio.h>

#define HEX_STEP 0x01

/**
 * Initializes the LED
 */
void setAsOutput() {
	DDRB |= (1 << PB5);
}

/**
 * Turns on the LED
 */
void turnOn() {
	PORTB |= (1 << PB5);
}

/**
 * Turns off the LED
 */
void turnOff() {
	PORTB &= ~(1 << PB5);
}

/**
 * Toggles the current state of the LED
 */
void toggle() {
	PORTB ^= (1 << PB5);
}

/**
 * Flashes the LED for a certain amount of time in 0.5 seconds interval
 */
void flash(uint8_t amount) {
	LED.turnOff();
	for (uint8_t i = 0x0; i < amount; i += HEX_STEP) {
		LED.toggle();
			_delay_ms(250);
		LED.toggle();
			_delay_ms(250);
	}
	LED.turnOff();
}

/**
 * Define the structure with the functions
 */
const struct LED LED = {
		.setAsOutput = setAsOutput,
		.turnOn = turnOn,
		.turnOff = turnOff,
		.toggle = toggle,
		.flash = flash
};
