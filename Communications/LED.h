/*
 * LED.h
 *
 *  Created on: 14 Nov 2016
 *      Author: asolis
 */

#ifndef COMMUNICATIONS_LED_H_
#define COMMUNICATIONS_LED_H_

#include <avr/io.h>
#include <util/delay.h>

/* LED communicator for PORTB PB5 */
struct LED {
	void (*setAsOutput)();
	void (*turnOn)();
	void (*turnOff)();
	void (*toggle)();
	void (*flash)(uint16_t amount);
};
extern const struct LED LED; //expose the LED structure

#endif /* COMMUNICATIONS_LED_H_ */
